package com.cjdevcode;

import java.util.ArrayList;
import java.util.List;

import com.cjdevcode.algorithms.Sorts;
import com.cjdevcode.example.implementations.Animal;
import com.cjdevcode.example.implementations.AnimalSorter;
import com.cjdevcode.example.implementations.IntListObject;
import com.cjdevcode.example.implementations.StringListObject;
import com.cjdevcode.interfaces.ListObjectInterface;

public class Test {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("CJDEVCODE_CMD_PROCESS_PRINT", "true");
		
		List<ListObjectInterface> strings = new ArrayList<>();
		strings.add(new StringListObject("dat"));
		strings.add(new StringListObject("at"));
		strings.add(new StringListObject("eat"));
		strings.add(new StringListObject("at"));
		strings.add(new StringListObject("bat"));
		
		List<ListObjectInterface> animals = new ArrayList<>();
		animals.add(new AnimalSorter(Animal.CAT));
		animals.add(new AnimalSorter(Animal.HORSE));
		animals.add(new AnimalSorter(Animal.MOUSE));
		animals.add(new AnimalSorter(Animal.DOG));
		animals.add(new AnimalSorter(Animal.CAT));
		animals.add(new AnimalSorter(Animal.RABBIT));
		
		List<ListObjectInterface> numbers = new ArrayList<>();
		numbers.add(new IntListObject(1));
		numbers.add(new IntListObject(5));
		numbers.add(new IntListObject(2));
		numbers.add(new IntListObject(3));
		numbers.add(new IntListObject(10));
//		numbers.add(new AnimalSorter(Animal.RABBIT));
//		numbers.add(new StringListObject("Aat"));
		numbers.add(new IntListObject(300));
		for(int i = 1980; i > 99; i--) {
			numbers.add(new IntListObject(i));
		}
		System.out.println(numbers.toString());
		long startTime = System.nanoTime();
		Sorts.radixSort(numbers);
//		Sorts.bubbleSort(numbers);
//		Sorts.insertionSort(numbers);
		long finishTime = System.nanoTime();
		System.out.println((finishTime-startTime));

//		System.out.println(numbers.toString());
//		Sorts.insertionSort(strings);
//		Sorts.insertionSort(animals);
		
//		Sorts.bubbleSort(numbers);
	}
}
