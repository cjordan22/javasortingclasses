package com.cjdevcode.algorithms;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.cjdevcode.interfaces.ListObjectInterface;

public class Sorts {
	
	static Boolean cmdProcessPrint = Boolean.valueOf(System.getProperty("CJDEVCODE_CMD_PROCESS_PRINT","false"));
	
	public List<ListObjectInterface> mergeSort(List<ListObjectInterface> preSortedListOne, List<ListObjectInterface> preSortedListTwo){
		return preSortedListOne;		
	}
	
	/**
	 * Radixsort
	 */
	public static List<ListObjectInterface> radixSort(List<ListObjectInterface> unsortedList){
		List<ListObjectInterface> sortedList = unsortedList;
		
		if(cmdProcessPrint) {
			System.out.println("Radixsort Start!");
			System.out.println("Starting String: " + sortedList.toString());
		}
		
		int numberOfBucketThrows = 1;
		Queue<ListObjectInterface>[] queues = new Queue[10];
		for(int bucketThrow = 1; bucketThrow <= numberOfBucketThrows;  bucketThrow++) {
			for(int i = 0; i < sortedList.size(); i++) {
				String stringRep = String.valueOf(sortedList.get(i).getValue());
				if(stringRep.length() > numberOfBucketThrows) {
					numberOfBucketThrows = stringRep.length();
				}
				int value = 0;
				try{
//					System.out.println(String.valueOf(stringRep.substring(stringRep.length()-bucketThrow),stringRep.length()-bucketThrow+1));
//					System.out.println(stringRep.length()-bucketThrow);
					value = Integer.parseInt(stringRep.substring(stringRep.length()-bucketThrow,stringRep.length()-bucketThrow+1));
				}
				catch(IndexOutOfBoundsException e) {
					
				}
//				System.out.println(stringRep + " " + value);
				if(queues[value] == null) {
					queues[value] = new LinkedList();
				}
				queues[value].add(sortedList.get(i));
			}
			if(cmdProcessPrint) {
				System.out.println("Finished Bucket Filling Round!");
			}
			sortedList = new ArrayList<ListObjectInterface>();
			for(int i = 0; i < 10; i ++) {
				if(queues[i] != null) {
					while(!queues[i].isEmpty()) {
						sortedList.add(queues[i].remove());
					}
				}
			}
			if(cmdProcessPrint) {
				System.out.println("Current list: " + sortedList.toString());
			}
		}
		
		if(cmdProcessPrint) {
			System.out.println("Sorted String: " + sortedList.toString());
			System.out.println("Radixsort End!");
		}
		return sortedList;
	}
	
	/**
	 * Bubblesort
	 * 
	 * 
	 * @param unsortedList
	 */
	public static void bubbleSort(List<ListObjectInterface> unsortedList){
		if(cmdProcessPrint) {
			System.out.println("Bubblesort Start!");
			System.out.println("Starting String: " + unsortedList.toString());
		}
		
		for(int i = unsortedList.size(); i > 0; i--) {
			for(int j = 0; j < i-1;j++) {
				if(unsortedList.get(j).getValue() > unsortedList.get(j+1).getValue()) {
					if(cmdProcessPrint) {
						System.out.println("Swapping Value: " + unsortedList.get(j) + " With: " + unsortedList.get(j+1));
					}
					unsortedList.add(j,unsortedList.get(j+1));
					unsortedList.remove(j+2);
					if(cmdProcessPrint) {
						System.out.println("New String: " + unsortedList.toString());
					}
				}
			}
			if(cmdProcessPrint) {
				System.out.println("Iteration "+(unsortedList.size() - i)+ " done");
			}
		}
		
		if(cmdProcessPrint) {
			System.out.println("Sorted String: " + unsortedList.toString());
			System.out.println("Bubblesort End!");
		}
	}
	
	/**
	 * 
	 * Insertion Sort scans through the list, takes the next index that is not sorted, and compares it to the sorted entries. 
	 * It scans through the sorted entries backwards. When it finds its sorted location, it shifts list at the current index to the right and inserts itself
	 * at the current index. Because it goes through the list backwards, it is a stable sort.
	 * 
	 * <br><br>
	 * Because we are using Lists instead of Arrays, the operations aren't 100% optimal.
	 * <br>
	 * Best Case: O(n)
	 * Average Case: O(n^2)
	 * Worst Case: O(n^2)
	 * 
	 * 
	 * @param unsortedList
	 */
	public static void insertionSort(List<ListObjectInterface> unsortedList){
		if(cmdProcessPrint) {
			System.out.println("Starting String: " + unsortedList.toString());
		}
		for(int i = 0; i < unsortedList.size(); i++) {
			if(cmdProcessPrint) {
				System.out.println("Current Index: " +i+" Value: " + unsortedList.get(i).toString());
			}
			int curValue = unsortedList.get(i).getValue();
			int j = i -1;
			while( j >= 0 && unsortedList.get(j).getValue() > curValue) {
				if(cmdProcessPrint) {
					System.out.println("Moving Before Index: " +j+" Value: " + unsortedList.get(j).toString());
				}
				j--;
			}
			if(unsortedList.get(j+1).getValue() != curValue) {
				unsortedList.add(j+1,unsortedList.get(i));
				unsortedList.remove(i+1);
				if(cmdProcessPrint) {
					System.out.println("Changed String: " + unsortedList.toString());
				}
			}else {
				if(cmdProcessPrint) {
					System.out.println("No Change: " + unsortedList.toString());
				}
			}
		}
		if(cmdProcessPrint) {
			System.out.println("Sorted String: " + unsortedList.toString());
		}
	}
	
	public void quickSort(List<ListObjectInterface> unsortedList){
	
	}
}
