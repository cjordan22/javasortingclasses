package com.cjdevcode.example.implementations;

public enum Animal {
	CAT,
	DOG,
	HORSE,
	MOUSE,
	RABBIT
}
