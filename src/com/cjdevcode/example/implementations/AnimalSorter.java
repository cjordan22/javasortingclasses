package com.cjdevcode.example.implementations;

import com.cjdevcode.interfaces.ListObjectInterface;

public class AnimalSorter extends ListObjectInterface{

	Animal value;
	
	public AnimalSorter(Object o) {
		super(o);
		value = (Animal) o;
	}

	@Override
	public int getValue() {
		switch(value) {
			case CAT:
				return 1;
			case DOG:
				return 2;
			case HORSE:
				return 8;
			case MOUSE:
				return 5;
			default:
				return 0;
		}
	}

	@Override
	public String toString() {
		return value.toString();
	}

}
