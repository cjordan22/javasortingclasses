package com.cjdevcode.example.implementations;

import com.cjdevcode.interfaces.ListObjectInterface;

public class IntListObject extends ListObjectInterface{

	int value;
	
	public IntListObject(Object o) {
		super(o);
		value = (int) o;
	}

	@Override
	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

}
