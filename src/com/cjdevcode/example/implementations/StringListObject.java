package com.cjdevcode.example.implementations;

import com.cjdevcode.interfaces.ListObjectInterface;

public class StringListObject extends ListObjectInterface{

	String value;
	
	public StringListObject(Object o) {
		super(o);
		value = (String) o;
	}
	
	@Override
	public int getValue() {
		return Character.valueOf(value.charAt(0));
	}

	@Override
	public String toString() {
		return value.toString();
	}
}